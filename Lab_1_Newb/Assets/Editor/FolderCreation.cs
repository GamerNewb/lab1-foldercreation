﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class FolderCreation : MonoBehaviour
{

    [MenuItem("Game Object/Create Empty")]
    public static void CreateFolder()
    {
        // This is all the info for the Dynamic Assets folder
        #region Dynamic Assets
        AssetDatabase.CreateFolder("Assets", "Dynamic Assets");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets", "Resources");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Animations");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Animations", "Sources");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Animation Controllers");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Effects");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Models");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Models", "Character");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Models", "Enviornment");
        System.IO.File.WriteAllText(Application.dataPath + "Dynamic Assets/Resources/Models/folderStructure", "The Models folder is for raw" +
            " models. These are for single looping-animation modles or for non-animated models");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Prefabs");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Prefabs", "Common");
        System.IO.File.WriteAllText(Application.dataPath + "Dynamic Assets/Resources/Prefabs/folderStructures.txt", "The Prefabs folder" +
            " is for all prefabs. A new folder should be created for each level/world/scene the prefab is used in. Any prefab used in " +
            "multiple levels/worlds/scenes should be placed in the common folder. Only one copy of each prefab should exist.");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Sounds");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds", "Music");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds/Music", "Common");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds", "SFX");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds/SFX", "Common");
        System.IO.File.WriteAllText(Application.dataPath + "/Dynamic Assets/Resources/Sounds/folderStructure.txt", "The sounds folder" + 
            " is for all sounds. It should follow the same organization as the prefabs folder.");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Textures");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Textures", "Common");
        System.IO.File.WriteAllText(Application.dataPath + "Dynamic Assets/Resources/Textures/folderStructures.txt", "The Textures " +
            "folder is for all the textures that are applied separately to models or changed at run-time");
        #endregion

        // This is all the info for the Extensions folder
        #region Extensions
        AssetDatabase.CreateFolder("Assets", "Extensions");
        System.IO.File.WriteAllText(Application.dataPath + "/Extensions/folderStructure.txt", " This is a folder for " +
            "third party Assets such as asset packages. The exception to this is the standard assets folder. The standard assets " +
            "folder should be left in the default location");
        #endregion

        // This is all the info for the Gizmos folder
        #region Gizmos
        AssetDatabase.CreateFolder("Assets", "Gizmos");
        System.IO.File.WriteAllText(Application.dataPath + "/Gizmos/folderStructure.txt", "This is a folder for gizmo scripts");
        #endregion

        // This is all the info for the Plugins folder
        #region Plugins
        AssetDatabase.CreateFolder("Assets", "Plugins");
        System.IO.File.WriteAllText(Application.dataPath + "/Plugins/folderStructure.txt", "This is a folder for plugin scripts");
        #endregion

        // This is all the info for the Scripts folder
        #region Scripts
        AssetDatabase.CreateFolder("Assets", "Scripts");
        System.IO.File.WriteAllText(Application.dataPath + "/Scripts/folderStructure.txt", "This is a folder for all other scripts." +
            " It should be seperated by common scripts found across multiple objects, and then scripts by level or by type.");
        #endregion

        // This is all the info for the Shaders folder
        #region Shaders
        AssetDatabase.CreateFolder("Assets", "Shaders");
        System.IO.File.WriteAllText(Application.dataPath + "/Shaders/folderStructure.txt", "This is a folder for all shader scripts." +
            " There is no inherent orginational structure for this folder.");
        #endregion

        // This is all the info for the Static Assets folder
        #region Static Assets
        AssetDatabase.CreateFolder("Assets", "Static Assets");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Animations");
        AssetDatabase.CreateFolder("Assets/Static Assets/Animations", "Sources");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Animation Controller");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Effects");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Models");
        AssetDatabase.CreateFolder("Assets/Static Assets/Models", "Character");
        AssetDatabase.CreateFolder("Assets/Static Assets/Models", "Enviornment");
        System.IO.File.WriteAllText(Application.dataPath + "Static Assets/Models/folderStructure", "The Models folder is for raw" +
            " models. These are for single looping-animation modles or for non-animated models");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Prefabs");
        AssetDatabase.CreateFolder("Assets/Static Assets/Prefabs", "Common");
        System.IO.File.WriteAllText(Application.dataPath + "Static Assets/Prefabs/folderStructures.txt", "The Prefabs folder" +
            " is for all prefabs. A new folder should be created for each level/world/scene the prefab is used in. Any prefab used in " +
            "multiple levels/worlds/scenes should be placed in the common folder. Only one copy of each prefab should exist.");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Scenes");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Sounds");
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds", "Music");
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds/Music", "Common");
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds", "SFX");
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds/SFX", "Common");
        System.IO.File.WriteAllText(Application.dataPath + "/Static Assets/Sounds/folderStructure.txt", "The sounds folder" +
            " is for all sounds. It should follow the same organization as the prefabs folder.");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Textures");
        AssetDatabase.CreateFolder("Assets/Static Assets/Textures", "Common");
        System.IO.File.WriteAllText(Application.dataPath + "Static Assets/Textures/folderStructures.txt", "The Textures " +
           "folder is for all the textures that are applied separately to models or changed at run-time");
        #endregion

        // This is all the info for the Testing folder
        #region Testing
        AssetDatabase.CreateFolder("Assets", "Testing");
        #endregion

        AssetDatabase.Refresh();

/*

        Post-Lab Questions
        
        #1. The Editor folder allows you to create custom inspectors and scripts in the folder allows Unity to run scripts that 
        don't pertain to the game you are currently creating.

        #2. [MenuItem("Game Object/Create Empty")] string/args

        #3. You need to add using UnityEditor namespace. You also have to tell Unity that you are going to be editing the Unity 
        Editor itself.

        #4. You need to create a function immediately below it and declare it public static void.
        
        #5. Creating folders happens on the Hard Disk, you need to refresh unity's version of the project. 

        #6. Static content is published to regular files on your server and handled using the simplest methods available to the web server.
        Dynamic content is generated for you at the time you request the page.  The document you view exists only for you at that moment;  
        if viewed by someone else at the same time, or by you at a slightly different time, you could get something different.

        #7. They should be kept in different folders because in the dynamic assets folder, there is a second organizational structure.
		Also Resources should never go into a Static Assets folder. <- (didn't read that, just wise words via T.Mour.
		
		#8 I don't think I own a workflow so I should be exempt from this question, but if I had to answer I would say I would create the
		folders in the order that comes first for convenience? If I were playing with animations first, for whatever reason, I would format that
		folder first. I think that's a legit answer because I have no idea what else I could say :D.

*/
    }
}